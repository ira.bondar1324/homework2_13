"use strict";

const arrImg = document.querySelectorAll(".image-to-show");
const btnStop = document.querySelector(".stop");
const btnRepeat = document.querySelector(".repeat");
const p = document.querySelector(".timer");

function changeDisplay(){
    for (const elem of arrImg) {
        elem.style.display = "none";
    }
}

let count =0;
function changeImg (){
    btnStop.style.display = "block";
    btnRepeat.style.display ="block";
    changeDisplay();
    arrImg[count].style.display ="block";
    if(++count>=arrImg.length){
        count = 0;
        clearInterval(timer);
        timer = setInterval(changeImg,3000);
    }
}
let timer = setInterval(changeImg,3000);


btnStop.addEventListener("click",()=>{
    clearInterval(timer);
})

btnRepeat.addEventListener("click",()=>{
    timer = setInterval(changeImg,3000);
})