1. Опишіть своїми словами різницю між функціями setTimeout() і serInterval().
    Функція setTimeout() викликається коли нам потрібно один раз виконати дію через заданий проміжок часу, а serInterval() - виконувати одну і туж дію постійно, теж через заданий проміжок часу.
2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
    Миттєво не спрацює тому що, браузеру потрібен час на обробку коду та ще в цей момент в ньому може виконуватись багато задач, які також перешкоджають моментальному виконанню. 
3. Чому важливо не забувати викликати функцію clearInterval(), коди раніше створений цикл запуску вам вже не потрібен?
    Тому що постійно виконуватиметься код і буде тим самим підгружати браузер, та й навіщо нам виконання, якщо воно вже не потрібно, а буде тільки заважати?